#!/bin/bash

echo -e "Acredite em você\n"
echo "O sucesso é a soma de pequenos esforços repetidos dia após dia"
echo "Seja você sua maior expectativa"
echo "Durma com ideias acorde com atitudes"
echo "uma meta é um sonho com um prazo"
echo "Nossos fracassos, as vezes, são mais frutiferos do que os êxitos."
echo "Não coloque limite em seus sonhos, coloque fé"
echo "Se não puder fazer tudo, faça tudo que puder"
echo "faça o que puder, com o que tiver, onde estiver."
echo "A sua vontade pode mudar o mundo"
