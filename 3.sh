#!/bin/bash

echo -e "----------Para criar variáveis o próprio usuário pode definir no script\nComo: a=10, b='oi'"
echo -e "----------Também pode pedir para digitar o valor da variavel com o comando read.\nFicando: read -p '"escreva algo: "'teste"
echo "-----------A diferença entre pedir o valor ao usuário ou já está escrito no script é que as vezes o 'valor' da variável irá mudar de acordo com a necessidade e não tem como advinhar qual será o 'valor'"
